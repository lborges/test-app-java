package com.codavel.testbolinaimport;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import com.codavel.bolina.proxy.BolinaConfiguration;
import com.codavel.bolina.proxy.ProxySingleton;
import com.codavel.lib.Network;


public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BolinaConfiguration configuration = new BolinaConfiguration.Builder()
                .withDefaultConfigTo(BolinaConfiguration.Builder.Mode.OKHTTP3_INTERCEPTOR)
                .withCertsEnabled(false)
                .withSelfSignedCertificates(true).build();
        ProxySingleton.startProxy(getApplicationContext(), configuration);


        ((Button) findViewById(R.id.button)).setOnClickListener(view -> {


            System.out.println("button");

            String port = ProxySingleton.getInstance().bolinaProxyPort;
            String token = ProxySingleton.getInstance().bolinaProxyToken;

            System.out.println("port: " + port);
            System.out.println("token: " + token);

            Network network = new Network(Integer.parseInt(port), token, configuration.interceptorConfig());
            network.doNetworkCall();
        });



    }






}
