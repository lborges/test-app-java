package com.codavel.lib;

import com.codavel.interceptor_okhttp3_java.CvlOkHttp3Interceptor;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class Network {

    private OkHttpClient client;

    public Network(int port, String token, String configuration) {

        client = new OkHttpClient.Builder()
                .addInterceptor(new CvlOkHttp3Interceptor(port,token, configuration))
                .build();

    }


    public void doNetworkCall() {

        //TODO: REPLACE <bolina server address> AND <network request url>
        Request request = new Request.Builder()
                .addHeader("Bolina-Address","")
                .url("<network request url>")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    ResponseBody responseBody = response.body();
                    if (!response.isSuccessful())
                        throw new IOException("Unexpected code " + response);

                    Headers responseHeaders = response.headers();
                    for (int i = 0, size = responseHeaders.size(); i < size; i++) {
                        System.out.println(responseHeaders.name(i) + ": " + responseHeaders.value(i));
                    }

                    System.out.println(responseBody.string());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }
}
